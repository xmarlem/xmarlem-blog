---
title: "Write Arm Template With Vscode"
date: 2020-09-28T20:15:04+02:00
draft: false
author: "xmarlem"
language: "en"
---

## GOAL: find an easy way to write ARM templates, with autocompletions and IDE support... 

Well, as I said in another post, I really like VSCODE, for being pretty lightweight and for all the available extensions! Together with intellij tools and VIM it's one of my favourite IDE tool.

### Pre-requisite

Install the following extensions:

<br><br>

- **ARM Tools**

![](/img/2020-09-28-20-18-34.png)

<br><br>



- **AZ CLI Tools**

![](/img/2020-09-28-20-19-14.png)


### Write your first template

Once done, simply create a new folder and a new ```json``` file:

![](/img/2020-09-28-20-20-33.png)

In order to generate the scheleton for an ARM template, simply write ```arm!```, the vscode extension provides a useful snippet:

![](/img/2020-09-28-20-21-49.png)

![](/img/2020-09-28-20-22-06.png)


In the resource section, type ```storage``. VSCODE will suggest another snippet:

![](/img/2020-09-28-20-24-22.png)

Once incorporated the snippet, you can move between the snippet parameters by hitting <TAB> and you can select among the different proposed options: 

![](/img/2020-09-28-20-25-37.png)

> **TIP!** you can always put your cursor on the parameter and hit <CTRL>+<SPACE> to get again the options

##### Parameters

It is possible to parametrize your template by filling the ```parameters``` section. Write "parameter" and vscode will suggest the snippet ```new-parameter```.
This is what you get:

![](/img/2020-09-28-20-29-06.png)


Here I added more parameters and given a name and description to the parameter:

![](/img/2020-09-28-20-30-54.png)

Last, but not the least, you will have to reference the parameter from the resource section:
![](/img/2020-09-28-20-32-13.png)  
<br><br>

#### Deploy the template via ```az cli```  

Once logged in and set your subscription... you can deploy the template with this command: 

{{<highlight bash>}}
 az deployment group create --resource-group mlrg --template-file testarm/arm-storageaccount.json --parameters testarm/arm-storageaccount.parameters.json
{{< /highlight >}}


> **Note:*** I created also a parameter file to provide a value to the parameters, but it was not required since the default value was already specified in the template.


