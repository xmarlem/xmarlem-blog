---
title: "Filter Json with JQ"
date: 2020-09-30T22:39:33+02:00
draft: false
author: "xmarlem"
language: "en"
tags: ["json", "azure", "jq"]
---

## GOAL: filter a json response with jq

One of the best and lightweight tools to filter a json message or file is definitely: JQ.

### Installation

> On Mac: `brew install jq`

> Other platforms: check here https://stedolan.github.io/jq/download/

### Use case

After i login with `azure`, and before creating a resource group, I usually wants to check the location names (which I definitely don't remember by hearth).

**That's a perfect use case for az cli and jq!!!**

Here we go, just type the command below:
```bash
az account list-locations | jq '.[] | .name | select(startswith("aus"))'
```

<br><br>



#### Explanation:
if you trigger ```az account list-locations``` you get the full json.
What i need to know and to filter is the `name` field.
Fine, I can simply feed that to jq command via pipe.

- .[] gives all values in the array listed 
- .name filter: selects the json element by extracting the name
- last filter `select(startswith("text")): select the items which make the condition `startswith("text") true! 

The result is a pure `SELECT name FROM json WHERE name like 'text%'`.

<br><br>


#### Reference: 
Official website -->  https://stedolan.github.io/jq/