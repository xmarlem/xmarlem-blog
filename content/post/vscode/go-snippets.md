---
title: "Go Snippets"
date: 2020-09-26T11:41:11+02:00
draft: false
author: "xmarlem"
language: "en"
tags: ["go", "vscode"]
myVar: "ecco"
---

## GOAL: configure go snippets in VSCode

What is a snippet? Just a chunk of code you would like to recall quickly whenever you type.

In ```vscode``` it's pretty simple to do that!


> for Windows,
go to --> ```File``` --> ```Preferences``` --> ```User snippets```

> for Mac,
go to --> ```Code``` --> ```Preferences``` --> ```User snippets```

then select `go.json` and edit the file.


Where the format is:

- **prefix**: the shortcut you will use before hitting <TAB>
- **body**: the chunk of code; please note that you can also specify the variables in the snippet. That way you will be able to tab between them when editing the snippet.
- **description**: a short description for the snippet here! :)

 
```json
	"For range statement": {
		"prefix": "forr",
		"body": "for ${1:_, }${2:var} := range ${3:var} {\n\t$0\n}",
		"description": "Snippet for a for range loop"
	},
```


Here is my ```go.json```, ENJOY! ;) :

{{< gist xmarlem bea2940fd8db48e87135ffc5ecbabb80 >}}