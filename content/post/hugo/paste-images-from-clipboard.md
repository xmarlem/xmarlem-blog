---
title: "HUGO: Paste images from clipboard into MD files"
date: 2020-09-26T09:21:38+02:00
draft: false
author: "xmarlem"
language: "en"
tags: [ "go", "customization" ]
---
#### GOAL: take a screenshot and paste it directly to your Markdown file in ```HUGO```

Well, I am using vscode and feel pretty comfortable with that, at least when writing static content.

Hence, my suggestion would be to use a vscode extension called ```Paste Image```.

![](/img/2020-09-26-09-23-52.png)

Open your ```settings.json``` conf file and add the following content:

```json
"pasteImage.path": "${projectRoot}/static/img",
"pasteImage.basePath": "${projectRoot}/static",
"pasteImage.forceUnixStyleSeparator": true,
"pasteImage.prefix": "/"
```

and that's it!!

**You can try it now...**

> on Mac:

```SHIFT + CTRL + CMD + 4``` 
--> take your screenshot and ```OPTION + CMD + V```  