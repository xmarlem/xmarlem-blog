+++
title = "About"
date = 2015-04-03T02:13:50Z
author = "xmarlem"
description = "Things about me."
+++

## About me

I am Marco Lembo, I live in Zurich, one of the best cities in the world! 

I work for a leading swiss :bank: as... > **IT Application Architect / SWC Manager**.

> ```My study interests are```: Software Engineering, Computer Networks, Radar and electro-optical sensors, Command and Control systems, Cloud computing, DevOps, Microservices.

> ```Specialties```: coding, devops, software engineering and architectures, design patterns and agile metodologies.
I absolutely love ```GOLANG``` which for me it's like going back to the childhood...when I started my journey with C lang.
I am musician too ... I can play the piano, as well as drums, guitar and bass guitar. I also have fun with percussions!

> I my spare time, I try to catch up with new technologies even if not applied at work

**I strongly believe IT it's evolving continuosly, but the key principles are always the same! i.e. CAP theorem, SOLID, etc...**


## My Education

I have a ```Master's Degree``` in *Computer Engineering* (5 years course)

```Field Of Study```: *Computer Science Engineering*

```Grade```: *110 out of 110*

```Thesis in```: *Pattern Recognition and Artificial Intelligence*

...but, even if not relevant, I have also an `accounting` school diploma... which I have never used! :see_no_evil: :see_no_evil: :see_no_evil: 


## What does `xmarlem` mean??

I used to work for Ericsson... and that was my nickname. I liked it and I use it almost everywhere :) 

## Why this blog??

Mostly for selfish reasons. I needed to keep track of my techie stuff somewhere...and thought to share the same with the community.
Hopefully other people will find these posts helpful, relevant, or interesting.
Nevertheless, I am not pretending to be an expert in all these topics, but time to time, by doing PoCs, learning new stuff, solving issues at work... I will be posting gists, tips & tricks or even technical discussions here. Stay tuned! :smile:

## Languages

Italian :it: , English :uk: , German (A1) :de:
